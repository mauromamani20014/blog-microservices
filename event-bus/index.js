const express = require("express");
const axios = require("axios");

const app = express();

app.use(express.json());

const events = [];

app.post("/events", (req, res) => {
  const event = req.body;

  events.push(event);

  axios.post("http://posts-clusteip-service:4000/events", event).catch(e => console.log(e));
  axios.post("http://comments-service:4001/events", event).catch(e => console.log(e));
  axios.post("http://query-service:4002/events", event).catch(e => console.log(e));
  axios.post("http://moderation-service:4003/events", event).catch(e => console.log(e));

  res.send({ status: "ok" });
});

app.get("/events", (req, res) => {
  res.send(events);
});

app.listen(4005, () => {
  console.log("Event bus listen on :4005");
});