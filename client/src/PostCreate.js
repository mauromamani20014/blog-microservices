import React, { useState } from 'react'
import axios from 'axios';

const PostCreate = () => {
  const [title, setTitle] = useState('');

  const handleSubmitForm = async (e) => {
    e.preventDefault();

    const response = await axios.post("http://posts.com/posts/create", { title });
    console.log(response.data);

    setTitle('');
  };

  return <div>
    <form onSubmit={handleSubmitForm}>
      <div className='form-group'>
        <label>Title</label>
        <input className='form-control' value={title} onChange={({ target }) => setTitle(target.value)} />
      </div>
      <button className='btn btn-primary'>Submit</button>
    </form>
  </div>
}

export default PostCreate;