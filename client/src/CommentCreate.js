import React, { useState } from "react";
import axios from "axios";

const CommentCreate = ({ postId }) => {
  const [newComment, setNewComment] = useState('');

  const handleSubmitForm = async (ev) => {
    ev.preventDefault();

    const response = await axios.post(`http://posts.com/posts/${postId}/comments`, { content: newComment });
    console.log(response.data);

    setNewComment('');
  }

  return <>
    <form onSubmit={handleSubmitForm}>
      <div className="form-group">
        <label>New Comment</label>
        <input value={newComment} className="form-control" onChange={({ target }) => setNewComment(target.value)} />
      </div>

      <button className="btn btn-primary">Submit</button>
    </form>
  </>
}

export default CommentCreate;