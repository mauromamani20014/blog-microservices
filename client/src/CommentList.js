import React, { useState, useEffect } from "react";
// import axios from "axios";

/**
 *  Lo comentado es una manera MUY ineficiente de hacer fetch de los datos, ya que por cada post se realizará una peticion.
 *  Lo solucionamos usando el micro-servicio de query
 */

const CommentList = ({ comments }) => {
  // const [comments, setComments] = useState([]);

  // const getComments = async () => {
  //   const response = await axios.get(`http://192.168.1.65:4001/posts/${postId}/comments`);
  //   setComments(response.data);
  // };

  // useEffect(() => {
  //   getComments();
  // }, []);

  const renderedComments = comments.map(c => {
    let content;

    if (c.status === "approved") {
      content = c.content;
    }

    if (c.status === "pending") {
      content = "this comment is awaiting moderation";
    }

    if (c.status === "rejected") {
      content = "this comment has been rejected";
    }

    return <li key={c.id}>{content}</li>
  });

  return <>
    <ul>
      {renderedComments}
    </ul>
  </>
}

export default CommentList;