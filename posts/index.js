const express = require("express");
const cors = require("cors");
const { randomBytes } = require("crypto");
const axios = require("axios");

const app = express();

app.use(express.json());
app.use(cors())

const posts = {};

// get all posts
app.get("/posts", (req, res) => {
  res.send(posts);
});

// create a post
app.post("/posts/create", async (req, res) => {
  const id = randomBytes(4).toString('hex');

  posts[id] = {
    id,
    title: req.body.title,
  };

  // emmit event
  await axios.post("http://event-bus-service:4005/events", {
    type: 'PostCreated',
    data: {
      id,
      title: req.body.title,
    }
  })

  res.status(201).send(posts[id]);
});

// receive event
app.post("/events", (req, res) => {
  console.log("EVENT: ", req.body.type);
  res.send({});
});

app.listen(4000, () => {
  console.log("Version: 0.1.0");
  console.log("Post service listening on 4000");
})