const express = require("express");
const cors = require("cors");
const { randomBytes } = require("crypto");
const axios = require("axios");

const app = express();

app.use(express.json());
app.use(cors());

const commentsByPostId = {};

// get all comments by specified post
app.get("/posts/:id/comments", (req, res) => {
  res.send(commentsByPostId[req.params.id] || []);
});

// create a comment
app.post("/posts/:id/comments", async (req, res) => {
  const id = randomBytes(4).toString('hex');
  const { content } = req.body;
  const postId = req.params.id;

  const comments = commentsByPostId[postId] || []

  comments.push({ id, content, status: 'pending' });

  commentsByPostId[postId] = comments;

  // emmit event
  await axios.post("http://event-bus-service:4005/events", {
    type: 'CommentCreated',
    data: {
      id,
      content,
      postId,
      status: 'pending',
    },
  });

  res.status(201).send(comments);
});

// receive event
app.post("/events", async (req, res) => {
  console.log("EVENT:");
  console.log(req.body);

  const { data, type } = req.body;

  if (type === "CommentModerated") {
    const comments = commentsByPostId[data.postId];
    const comment = comments.find(c => c.id === data.id);
    comment.status = data.status;

    // emmit event
    await axios.post("http://event-bus-service:4005/events", {
      type: 'CommentUpdated',
      data: {
        id: data.id,
        status: data.status,
        content: data.content,
        postId: data.postId,
      },
    });
  }

  res.send({});
});

app.listen(4001, () => {
  console.log("Comment service listening on 4001");
})